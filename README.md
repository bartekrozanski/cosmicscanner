# What is it
This program was made to help PhD student from Warsaw University in his PhD thesis in Physics.
It computes intersection points and changes in line directions of milions of lines in real time using nVidia CUDA and then renders it on the screen using CUDA-OpenGL interoperability.


# What thesis was about
Every second we are hit by some particles from space (cosmic radiation).

Similar radiation is used to perform x-rays, so maybe... Maybe we can use that continuous fire of space particles to perform scans of some objects?
It seems like we can!
If we place one detector of said particles on the ground, then - on top of it - we place an object we want to "x -ray" and after that we cover this construction with second detector like so:
```

  [SPACE]

\
 \
  *               <--- particle from space
============      <--- detector
    ----
    |XX|          <--- object we want to scan (whats inside? This is what we want to know)
    ----
============      <--- detector

```
then by taking into account how course of given particle has changed, we can visualize shape of an object between detectors.
These particles will change their course if they hit something different than air.

This process takes some time. These particles fall in rate one particle per square centimeter per second. To gather enough data, we have to leave an object between detectors for a few hours.

# Screenshots
This output was made using data from scanning solid block of concrete with an empty barrel inside.

<img src="screenshots/screenshot1.PNG" width="400" height="300" />
<img src="screenshots/screenshot2.PNG" width="400" height="300" />
<img src="screenshots/screenshot3.PNG" width="400" height="300" />

# Controls
Before running this program, you must run [CosmicScannerUI](https://gitlab.com/bartekrozanski/cosmicscannerui) first.

Use mouse to control camera.

Use UI to change positions of detectors, filter rendered data etc.

# Why UI is separate program?
At the beggining, the purpose of this program was to only calculate and render intersection points between courses of flight of detected particles.
Then it came out that having UI and possibility to move detectors around and filter rendered data basing on the intersection angle of courses or distance of two lines (In fact, data is not perect, so these line do not intersect. Program renders point which is the closest to both lines) is mandatory.

Because I wanted to learn something new about inter process communication, I created second program ([CosmicScannerUI](https://gitlab.com/bartekrozanski/cosmicscannerui)) which communicates with this one using Memory Mapped File.

# Building this project
It requires Windows (UI is written in winforms)

1. Clone this repo
2. Download [glew](http://glew.sourceforge.net/)
3. Place `glew` folder (delete version signature from directory name) in the root directory of cloned repo
2. Download [glfw](https://www.glfw.org/)
3. Place `glfw` folder (delete version signature from directory name) in the root directory of cloned repo
4. Download [glm](https://github.com/g-truc/glm/tags)
5. Place `glm` folder in the root directory of cloned repo
6. You are ready to go! Open root directory with Visual Studio, wait for CMake to do its job and build the project
7. (Optional) From default, project will build in debug mode. Switch to release mode in Visual Studio to greatly increase framerate.

# Launching
This program looks for memory mapped file created by [CosmicScannerUI](https://gitlab.com/bartekrozanski/cosmicscannerui).

Before running this program, run said UI.

This program also looks for detectors' data in root directory of this project.
Data can be downloaded [here](https://drive.google.com/file/d/1DPdOEp0X2lzR1g5_9YpsPvjeBlfmlY-E/view?usp=sharing).
Unpack downloaded data and place CosmicRadiationScanner directory in root directory of this repo.