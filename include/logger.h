#pragma once
#define logError(msg) (fprintf(stderr, "[ERROR]%s at line %d: %s\n",__FUNCSIG__, __LINE__ , msg))
#define logInfo(msg) (fprintf(stdout, "[INFO]%s at line %d: %s\n",__FUNCSIG__, __LINE__ , msg))
