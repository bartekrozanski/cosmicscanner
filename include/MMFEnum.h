#pragma once

#define Dist_ShowAll 0
#define Dist_GreaterThan 1
#define Dist_LessThan 2
#define Dist_InvalidValue -2.0f

#define Angle_ShowAll 0
#define Angle_Between 1
#define Angle_NotBetween 2
#define Angle_InvalidValue -2.0f


enum VariablesPositions
{
	DataChanged,
	Sensor0X,
	Sensor0Y,
	Sensor0Z,
	Sensor1X,
	Sensor1Y,
	Sensor1Z,
	Sensor2X,
	Sensor2Y,
	Sensor2Z,
	Sensor3X,
	Sensor3Y,
	Sensor3Z,
	DistancePredicateType,
	DistancePredicateLimit,
	SwapTop,
	SwapBottom,
	AnglePredicateType,
	AngleMin,
	AngleMax,
	Total
};
