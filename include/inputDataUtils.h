#pragma once
#include <stdio.h>
#include <stdlib.h>
#include "logger.h"

constexpr int BUFFER_SIZE = 1<<16;
int countLines(const char * path);
float* loadSensorData(const char *path, int dataCount);
