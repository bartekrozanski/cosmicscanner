#include "inputDataUtils.h"

float* loadSensorData(const char *path, int dataCount)
{
	size_t lines;
	lines = countLines(path);
	if (lines < 0)
	{
		logError("Could not count lines in file");
		logError(path);
		return NULL;
	}
	if (dataCount > lines)
	{
		logError("dataCount is bigger than lines available in file");
		return NULL;
	}
	FILE *file;
	file = fopen(path, "r");
	if (!file)
	{
		logError("Could not open file");
		logError(path);
		return NULL;
	}
	float *data = (float *)malloc(sizeof(float) * dataCount);
	if (!data)
	{
		logError("Could not allocate memory for data");
		return NULL;
	}
	size_t it = 0;
	while (it < dataCount)
	{
		fscanf(file, "%f\n", &data[it++]);
	}
	if (fclose(file) < 0)
	{
		logError("Could not properly close file");
		logError(path);
		return NULL;
	}
	return data;
}

int countLines(const char * path)
{
	FILE *file;
	char buffer[BUFFER_SIZE];
	int lineCount = 0;
	size_t read;

	file = fopen(path, "r");
	if (!file)
	{
		logError("Could not open file");
		logError(path);
		return -1;
	}
	while (read = fread(buffer, sizeof(char), BUFFER_SIZE, file))
	{
		while (read--)
		{
			if (buffer[read] == '\n') lineCount++;
		}
	}
	if (fclose(file) < 0)
	{
		logError("Could not properly close file");
		logError(path);
		return -1;
	}
	return lineCount;
}