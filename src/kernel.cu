#define _CRT_SECURE_NO_WARNINGS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdlib.h>

#include "logger.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "cuda_runtime.h"
#include <cuda_gl_interop.h>
//#include <helper_cuda.h>         // helper functions for CUDA error check

#include "device_launch_parameters.h"

#include "inputDataUtils.h"
#include "MMFEnum.h"
#include <direct.h>

//MemoryMappedFiles
#include <windows.h>
#include <conio.h>

void synchronizeVariables(float *MMF);

//#include <tchar.h>



//window config
int gl_windowW = 800;
int gl_windowH = 600;
const char *title = "Usmiechnij sie :)";

/*
float x
float y
float z
float cos
float dist
*/


//camera
glm::vec3 cameraPos = glm::vec3(3.0f, 3.0f, 3.0f);
glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
glm::mat4 getViewMatrix();


//mouse
int left_button_down = 0;
int right_button_down = 0;

float rotate_x=0;
float rotate_y=0;
float translate_z =0;

double lastX, lastY;
float yaw = 0;
float pitch = 0;
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);


//sensors
typedef struct Sensor {
	glm::vec3 pos;
	float *dataX;
	float *dataY;
	float *ptr_to_dev_dataX;
	float *ptr_to_dev_dataY;
} Sensor;

int dataCount;
constexpr int sensorCount = 4;
Sensor sensors[sensorCount];
int initSensorsData(Sensor *sensors, int dataCount);
void swapSensorsDevicePointers(Sensor *one, Sensor * other);

int distancePredicate;
float distancePredicateLimit;
int anglePredicate;
float angleMax;
float angleMin;

void processInput(GLFWwindow *window);
GLFWwindow* initOpenGL(int width, int height, const char * title);
GLuint compileShader(const char * path, int shaderType);
GLuint createShaderProgram(const char * vertexShaderPath, const char * fragmentShaderPath);


//CUDA
constexpr int threadsPerBlock = 512;
void launchKernel(float * mappedResourcePtr);
struct cudaGraphicsResource *cuda_vbo_resource;
void runCuda(struct cudaGraphicsResource **vbo_resource);
cudaError_t initCuda();

__device__ float3 subtract(float3 a, float3 b)
{
	float3 c;
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;
	return c;
}
__device__ float3 add(float3 a, float3 b)
{
	float3 c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	c.z = a.z + b.z;
	return c;
}
__device__ float3 scalar(float s, float3 a)
{
	float3 c;
	c.x = s * a.x;
	c.y = s * a.y;
	c.z = s * a.z;
	return c;
}
__device__ float dot(float3 a, float3 b)
{
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

__global__ void myKernel(
	float sensor0X,
	float sensor0Y,
	float sensor0Z,
	float sensor1X,
	float sensor1Y,
	float sensor1Z,
	float sensor2X,
	float sensor2Y,
	float sensor2Z,
	float sensor3X,
	float sensor3Y,
	float sensor3Z,
	float * sensor0Xdata,
	float * sensor0Ydata,
	float * sensor1Xdata,
	float * sensor1Ydata,
	float * sensor2Xdata,
	float * sensor2Ydata,
	float * sensor3Xdata,
	float * sensor3Ydata,
	int dataCount,
	float* mappedResourcePtr,
	int distPredicate,
	float distLimit,
	int anglePredicate,
	float angleMin,
	float angleMax
	)
{
	int n = blockIdx.x * blockDim.x + threadIdx.x;
	if (n < dataCount)
	{
		float3 P[4], inTrajectoryVec, outTrajectoryVec, alfa, w, output, xl, xk;
		P[0].x = sensor0Xdata[n] + sensor0X;
		P[0].y = sensor0Ydata[n] + sensor0Y;
		P[0].z = sensor0Z;

		P[1].x = sensor1Xdata[n] + sensor1X;
		P[1].y = sensor1Ydata[n] + sensor1Y;;
		P[1].z = sensor1Z;

		P[2].x = sensor2Xdata[n] + sensor2X;;
		P[2].y = sensor2Ydata[n] + sensor2Y;;
		P[2].z = sensor2Z;

		P[3].x = sensor3Xdata[n] + sensor3X;;
		P[3].y = sensor3Ydata[n] + sensor3Y;;
		P[3].z = sensor3Z;

		inTrajectoryVec = subtract(P[1], P[0]);
		outTrajectoryVec = subtract(P[3], P[2]);
		alfa = subtract(P[2], P[0]);
		float a, b, c, d, e, t, s, det, cos, dist, angle;
		a = dot(inTrajectoryVec, inTrajectoryVec);
		b = dot(inTrajectoryVec, outTrajectoryVec);
		c = -dot(outTrajectoryVec, outTrajectoryVec);
		d = dot(alfa, inTrajectoryVec);
		e = dot(alfa, outTrajectoryVec);
		det = a * c + b * b;
		t = c * d + e * b;
		t = t / det;
		s = a * e - b * d;
		s = s / det;

		xl = add(P[0], scalar(t, inTrajectoryVec));
		xk = add(P[2], scalar(s, outTrajectoryVec));
		w = subtract(xk, xl);
		output = add(xl, scalar(0.5f, w));

		cos = b / (sqrtf(a)*sqrtf(-c));
		angle = acosf(cos);

		dist = norm3df(w.x, w.y, w.z);
		mappedResourcePtr[n * 5] = output.x;
		mappedResourcePtr[n * 5 + 1] = output.y;
		mappedResourcePtr[n * 5 + 2] = output.z;

		if(anglePredicate == Angle_ShowAll) mappedResourcePtr[n * 5 + 3] = angle;
		else if (anglePredicate == Angle_Between)
		{
			mappedResourcePtr[n * 5 + 3] = (angle >= angleMin && angle <= angleMax) ? angle : Angle_InvalidValue;
		}
		else
		{
			mappedResourcePtr[n * 5 + 3] = (angle < angleMin || angle > angleMax) ? angle : Angle_InvalidValue;
		}
		

		if (distPredicate == Dist_ShowAll) mappedResourcePtr[n * 5 + 4] = dist;
		else if (distPredicate == Dist_GreaterThan) mappedResourcePtr[n * 5 + 4] = dist>distLimit ? dist : Dist_InvalidValue;
		else mappedResourcePtr[n * 5 + 4] = dist < distLimit ? dist : Dist_InvalidValue;
	}
}

int main()
{
	float sensorVertices[] = {
		-0.5f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
	};

	float axesVertices[] = {
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f
	};

	logInfo("Creating MemoryMappedFile");
	HANDLE hMapFile;
	char* MMF;
	hMapFile = OpenFileMapping(
		FILE_MAP_ALL_ACCESS,
		FALSE,
		"CosmicScanMMF"
	);
	if (hMapFile == NULL)
	{
		logError("Could not open file mapping object");
		return EXIT_FAILURE;
	}
	MMF = (LPTSTR)MapViewOfFile(
		hMapFile,
		FILE_MAP_ALL_ACCESS,
		0,
		0,
		VariablesPositions::Total * sizeof(float)
	);
	if (MMF == NULL)
	{
		logError("Could not map veiw of file");
		CloseHandle(hMapFile);
		return EXIT_FAILURE;
	}
	printf("Specify how many points to load:\n");
	scanf("%d", &dataCount);

	if (initSensorsData(sensors, dataCount) < 0) return EXIT_FAILURE;

	if (initCuda() != cudaSuccess) return EXIT_FAILURE;
	//INIT
	GLFWwindow *window = initOpenGL(gl_windowW, gl_windowH, title);
	if (!window)
	{
		logError("Could not initialize OpenGL");
		return EXIT_FAILURE;
	}
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);




	//SHADERS INIT
	GLuint sensorShaderProgram = createShaderProgram("../../../shaders/vertexShader", "../../../shaders/fragmentShader");
	if (!sensorShaderProgram)
	{
		logError("Could not create sensorShaderProgram");
		return EXIT_FAILURE;
	}

	GLuint axesShaderProgram = createShaderProgram("../../../shaders/axesVertexShader", "../../../shaders/axesFragmentShader");
	if (!axesShaderProgram)
	{
		logError("Could not create axesShaderProgram");
		return EXIT_FAILURE;
	}

	GLuint cloudShaderProgram = createShaderProgram("../../../shaders/cloudVertexShader", "../../../shaders/cloudFragmentShader");
	if (!cloudShaderProgram)
	{
		logError("Could not create cloudShaderProgram");
		return EXIT_FAILURE;
	}

	GLuint uniModelLoc, uniViewLoc, uniProjectionLoc;
	uniModelLoc = glGetUniformLocation(sensorShaderProgram, "model");
	uniViewLoc = glGetUniformLocation(sensorShaderProgram, "view");
	uniProjectionLoc = glGetUniformLocation(sensorShaderProgram, "projection");


	//Generate VAOs, VBOs
	GLuint sensorVAO, axesVAO, cloudVAO;
	glGenVertexArrays(1, &sensorVAO);
	glGenVertexArrays(1, &axesVAO);
	glGenVertexArrays(1, &cloudVAO);

	GLuint sensorVBO, axesVBO, cloudVBO;
	glGenBuffers(1, &sensorVBO);
	glGenBuffers(1, &axesVBO);
	glGenBuffers(1, &cloudVBO);


	//Bind VAOs, VBOs
	glBindVertexArray(sensorVAO);
	glBindBuffer(GL_ARRAY_BUFFER, sensorVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sensorVertices), sensorVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, (void *)(sizeof(float) * 0));
	glEnableVertexAttribArray(0);

	glBindVertexArray(axesVAO);
	glBindBuffer(GL_ARRAY_BUFFER, axesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(axesVertices), axesVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (void *)(sizeof(float) * 0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (void *)(sizeof(float) * 3));
	glEnableVertexAttribArray(1);

	glBindVertexArray(cloudVAO);
	glBindBuffer(GL_ARRAY_BUFFER, cloudVBO);
	glBufferData(GL_ARRAY_BUFFER, dataCount * sizeof(float) * 5, 0, GL_DYNAMIC_DRAW); //static_draw?
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void *)(sizeof(float) * 0));
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void *)(sizeof(float) * 3));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (void *)(sizeof(float) * 4));
	glEnableVertexAttribArray(2);

	cudaGraphicsGLRegisterBuffer(&cuda_vbo_resource, cloudVBO, cudaGraphicsMapFlagsWriteDiscard);

	//Unbind VAOs, VBOs
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 projection;
	projection = glm::perspective(glm::radians(45.0f), (float)gl_windowW / gl_windowH, 0.1f, 1000.0f);


	double lastTime = 0;
	int frames = 0;
	char buffer[32];

	synchronizeVariables((float*)MMF);
	runCuda(&cuda_vbo_resource);
	//MAIN LOOP
	while (!glfwWindowShouldClose(window))
	{
		if( ((float*)MMF)[VariablesPositions::DataChanged] )
		{
			synchronizeVariables((float*) MMF);
			runCuda(&cuda_vbo_resource);
		}

		//Measure ms per frame
		frames++;
		double currentTime = glfwGetTime();
		double delta = currentTime - lastTime;
		if (delta >= 1.0)
		{
			sprintf(buffer, "%.6lf", delta / (double)frames * 1000);
			glfwSetWindowTitle(window, buffer);
			frames = 0;
			lastTime = currentTime;
		}

		//HANDLE INPUT
		glfwPollEvents();
		processInput(window);

		//RENDER
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glUseProgram(axesShaderProgram);
		model = glm::mat4(1.0f);
		glUniformMatrix4fv(uniModelLoc, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(uniViewLoc, 1, GL_FALSE, glm::value_ptr(getViewMatrix()));
		glUniformMatrix4fv(uniProjectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
		glBindVertexArray(axesVAO);
		glDrawArrays(GL_LINES, 0, 6);


		//render sensors
		glUseProgram(sensorShaderProgram);
		glUniformMatrix4fv(uniViewLoc, 1, GL_FALSE, glm::value_ptr(getViewMatrix()));
		glUniformMatrix4fv(uniProjectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

		for (int i = 0; i < sensorCount; ++i)
		{
			model = glm::mat4(1.0f);
			model = glm::translate(model, sensors[i].pos);
			glUniformMatrix4fv(uniModelLoc, 1, GL_FALSE, glm::value_ptr(model));
			glBindVertexArray(sensorVAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}
		glUseProgram(cloudShaderProgram);
		glUniformMatrix4fv(uniViewLoc, 1, GL_FALSE, glm::value_ptr(getViewMatrix()));
		glUniformMatrix4fv(uniProjectionLoc, 1, GL_FALSE, glm::value_ptr(projection));
		model = glm::mat4(1.0f);
		model = glm::scale(model, glm::vec3(0.01f, 0.01f, 0.01f));
		glUniformMatrix4fv(uniModelLoc, 1, GL_FALSE, glm::value_ptr(model));
		glBindVertexArray(cloudVAO);
		glDrawArrays(GL_POINTS, 0, dataCount*3);

		glfwSwapBuffers(window);
	}
	

	//CLEANUP
	glfwTerminate();
	return EXIT_SUCCESS;
}


GLFWwindow* initOpenGL(int width, int height, const char * title)
{
	if (!glfwInit())
	{
		logError("Failed to initaialize GLFW");
		return NULL;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	GLFWwindow *window = glfwCreateWindow(width, height, title, NULL, NULL);
	if (window == NULL)
	{
		logError("Failed to create GLFW window");
		glfwTerminate();
		return NULL;

	}

	glfwMakeContextCurrent(window);
	glViewport(0, 0, width, height);
	glewExperimental = GL_TRUE;
	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	if (GLEW_OK != glewInit())
	{
		logError("Failed to initialize GLEW");
		return NULL;

	}
	glEnable(GL_DEPTH_TEST);
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	return window;
} 

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS)
	{
		Sensor tmp = sensors[0];
		sensors[0] = sensors[1];
		sensors[1] = tmp;
	}
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS)
	{
		Sensor tmp = sensors[2];
		sensors[2] = sensors[3];
		sensors[3] = tmp;
	}
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (left_button_down)
	{
		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos; // reversed since y-coordinates range from bottom to top
		lastX = xpos;
		lastY = ypos;
		float sensitivity = 0.15f;
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		glm::mat4 Identity = glm::mat4(1.0f);
		glm::mat4 RotatedX = glm::rotate(Identity, glm::radians(xoffset), glm::vec3(0.0f, 0.0f, 1.0f));
		glm::mat4 FullRotate = glm::rotate(RotatedX, glm::radians(yoffset), glm::cross(cameraPos, glm::vec3(0.0f, 0.0f, 1.0f)));
		glm::vec4 newPos = glm::vec4(cameraPos.x, cameraPos.y, cameraPos.z, 1.0f);
		newPos = FullRotate * newPos;
		cameraPos.x = newPos.x;
		cameraPos.y = newPos.y;
		cameraPos.z = newPos.z;
	}
	if (right_button_down)
	{
		float yoffset = lastY - ypos; // reversed since y-coordinates range from bottom to top
		lastY = ypos;
		float sensitivity = 0.15f;
		yoffset *= sensitivity;
		cameraPos += glm::normalize(cameraPos)*yoffset;
	}
}

GLuint createShaderProgram(const char * vertexShaderPath, const char * fragmentShaderPath)
{
	GLuint vertexShader = compileShader(vertexShaderPath, GL_VERTEX_SHADER);
	if (!vertexShader)
	{
		logError("Could not compile shader");
		return 0;
	}
	GLuint fragmentShader = compileShader(fragmentShaderPath, GL_FRAGMENT_SHADER);
	if (!fragmentShader)
	{
		logError("Could not compile shader");
		return 0;
	}
	GLuint shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	int success;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		logError(infoLog);
		return 0;
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	return shaderProgram;
}

GLuint compileShader(const char * path, int shaderType)
{
	int fail = 0;
	FILE *file;
	int read;
	file = fopen(path, "r");
	const char *buffer = (char*)malloc(sizeof(char) * 1024);
	if (!buffer)
	{
		logError("Could not allocate memory for buffer");
		fail = 1;
		goto cleanUp;
	}
	if (!file)
	{
		logError("Could not open file");
		logError(path);
		fail = 1;
		goto cleanUp;
	}

	read = fread((void*)buffer, sizeof(char), 1024, file);
	if (read < 0)
	{
		logError("Could not read from file");
		perror(NULL);
		fail = 1;
		goto cleanUp;
	}
	if (read >= 1023)
	{
		logError("Buffer is too small for file");
		logError(path);
		fail = 1;
		goto cleanUp;
	}


	GLuint shader;
	shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &buffer, &read);
	glCompileShader(shader);

	int success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		char infoLog[512];
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		logError("Shader compilation error");
		logError(infoLog);
		fail = 1;
		goto cleanUp;
	}

cleanUp:
	if (file)
	{
		if (fclose(file) < 0)
		{
			logError("Could not close file");
			logError(path);
			fail = 1;
		}
	}
	if (buffer)
	{
		free((void*)buffer);
	}
	if (fail) return 0;
	return shader;
}

void synchronizeVariables(float * MMF)
{
	sensors[0].pos.x = MMF[VariablesPositions::Sensor0X];
	sensors[0].pos.y = MMF[VariablesPositions::Sensor0Y];
	sensors[0].pos.z = MMF[VariablesPositions::Sensor0Z];
	sensors[1].pos.x = MMF[VariablesPositions::Sensor1X];
	sensors[1].pos.y = MMF[VariablesPositions::Sensor1Y];
	sensors[1].pos.z = MMF[VariablesPositions::Sensor1Z];
	sensors[2].pos.x = MMF[VariablesPositions::Sensor2X];
	sensors[2].pos.y = MMF[VariablesPositions::Sensor2Y];
	sensors[2].pos.z = MMF[VariablesPositions::Sensor2Z];
	sensors[3].pos.x = MMF[VariablesPositions::Sensor3X];
	sensors[3].pos.y = MMF[VariablesPositions::Sensor3Y];
	sensors[3].pos.z = MMF[VariablesPositions::Sensor3Z];
	distancePredicate = MMF[VariablesPositions::DistancePredicateType];
	distancePredicateLimit = MMF[VariablesPositions::DistancePredicateLimit];
	anglePredicate = MMF[VariablesPositions::AnglePredicateType];
	angleMin = MMF[VariablesPositions::AngleMin];
	angleMax= MMF[VariablesPositions::AngleMax];

	if (MMF[VariablesPositions::SwapBottom])
	{
		MMF[VariablesPositions::SwapBottom] = 0;
		swapSensorsDevicePointers(&sensors[0], &sensors[1]);
	}
	if (MMF[VariablesPositions::SwapTop])
	{
		MMF[VariablesPositions::SwapTop] = 0;
		swapSensorsDevicePointers(&sensors[2], &sensors[3]);
	}
	MMF[VariablesPositions::DataChanged] = 0;
}

glm::mat4 getViewMatrix()
{
	return glm::lookAt(cameraPos, cameraTarget, up);
}

int initSensorsData(Sensor *sensors, int dataCount)
{
	logInfo("Initializing sensor data");
	if (!(sensors[0].dataX = loadSensorData("../../../CosmicRadiationScanner/backup/x_RPC_above", dataCount))) goto error;
	if (!(sensors[0].dataY = loadSensorData("../../../CosmicRadiationScanner/backup/y_RPC_above", dataCount))) goto error;
	logInfo("First sensor data loaded");
	if (!(sensors[1].dataX = loadSensorData("../../../CosmicRadiationScanner/backup/x_DRIFT_above", dataCount))) goto error;
	if (!(sensors[1].dataY = loadSensorData("../../../CosmicRadiationScanner/backup/y_DRIFT_above", dataCount))) goto error;
	logInfo("Second sensor data loaded");
	if (!(sensors[2].dataX = loadSensorData("../../../CosmicRadiationScanner/backup/x_RPC_below", dataCount))) goto error;
	if (!(sensors[2].dataY = loadSensorData("../../../CosmicRadiationScanner/backup/y_RPC_below", dataCount))) goto error;
	logInfo("Third sensor data loaded");
	if (!(sensors[3].dataX = loadSensorData("../../../CosmicRadiationScanner/backup/x_DRIFT_below", dataCount))) goto error;
	if (!(sensors[3].dataY = loadSensorData("../../../CosmicRadiationScanner/backup/y_DRIFT_below", dataCount))) goto error;
	logInfo("Fourth sensor data loaded");
	return 0;
error:
	logError("Could not initialize sensors data");
	return -1;
}

cudaError_t initCuda()
{
	cudaError_t cudaStatus;
	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		logError("cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}
	for (int i = 0; i < sensorCount; ++i)
	{
		cudaStatus = cudaMalloc((void**)&(sensors[i].ptr_to_dev_dataX), dataCount * sizeof(float));
		if (cudaStatus != cudaSuccess) {
			logError("cudaMalloc failed");
			goto Error;
		}
		cudaStatus = cudaMalloc((void**)&(sensors[i].ptr_to_dev_dataY), dataCount * sizeof(float));
		if (cudaStatus != cudaSuccess) {
			logError("cudaMalloc failed");
			goto Error;
		}
	}
	for (int i = 0; i < sensorCount; ++i)
	{
		cudaStatus = cudaMemcpy(sensors[i].ptr_to_dev_dataX, sensors[i].dataX, dataCount * sizeof(float), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			logError("cudaMemcpy failed");
			goto Error;
		}
		cudaStatus = cudaMemcpy(sensors[i].ptr_to_dev_dataY, sensors[i].dataY, dataCount * sizeof(float), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			logError("cudaMemcpy failed");
			goto Error;
		}
	}
	logInfo("Successfuly loaded data to device");
	return cudaStatus;
Error:
	for (int i = 0; i < sensorCount; ++i)
	{
		cudaFree(sensors[i].ptr_to_dev_dataX);
		cudaFree(sensors[i].ptr_to_dev_dataY);
	}
	return cudaStatus;
}

void launchKernel(float * mappedResourcePtr)
{
	int blocks = (int)ceil((double)dataCount / threadsPerBlock);
	myKernel<<<blocks, threadsPerBlock >>>(
		sensors[0].pos.x,
		sensors[0].pos.y,
		sensors[0].pos.z,
		sensors[1].pos.x,
		sensors[1].pos.y,
		sensors[1].pos.z,
		sensors[2].pos.x,
		sensors[2].pos.y,
		sensors[2].pos.z,
		sensors[3].pos.x,
		sensors[3].pos.y,
		sensors[3].pos.z,
		sensors[0].ptr_to_dev_dataX,
		sensors[0].ptr_to_dev_dataY,
		sensors[1].ptr_to_dev_dataX,
		sensors[1].ptr_to_dev_dataY,
		sensors[2].ptr_to_dev_dataX,
		sensors[2].ptr_to_dev_dataY,
		sensors[3].ptr_to_dev_dataX,
		sensors[3].ptr_to_dev_dataY,
		dataCount,
		mappedResourcePtr,
		distancePredicate,
		distancePredicateLimit,
		anglePredicate,
		angleMin,
		angleMax
		);
}

void runCuda(struct cudaGraphicsResource **vbo_resource)
{
	float *dptr;
	cudaGraphicsMapResources(1, vbo_resource, 0);
	size_t num_bytes;
	cudaGraphicsResourceGetMappedPointer((void **)&dptr, &num_bytes,*vbo_resource);
	launchKernel(dptr);
	cudaGraphicsUnmapResources(1, vbo_resource, 0);
	cudaDeviceSynchronize();
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		glfwGetCursorPos(window, &lastX, &lastY);
		left_button_down = 1;
	}
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		left_button_down = 0;
	}
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		glfwGetCursorPos(window, &lastX, &lastY);
		right_button_down = 1;
	}
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
	{
		right_button_down = 0;
	}
}

void swapSensorsDevicePointers(Sensor *one, Sensor * other)
{
	Sensor tmp = (*one);
	one->dataX = other->dataX;
	one->dataY = other->dataY;
	one->ptr_to_dev_dataX = other->ptr_to_dev_dataX;
	one->ptr_to_dev_dataY= other->ptr_to_dev_dataY;
	other->dataX = tmp.dataX;
	other->dataY = tmp.dataY;
	other->ptr_to_dev_dataX = tmp.ptr_to_dev_dataX;
	other->ptr_to_dev_dataY = tmp.ptr_to_dev_dataY;
}
